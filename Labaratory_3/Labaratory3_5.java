package lab3fifthtask;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Labaratory3_5
{
	private static final String SUM = "SUM";

	 // Fifth task
	 public static void main(String[] args) throws IOException
	 {
	//  Examples:
	//  "1 2 SUM" --> 3
	//  "1 -2 SUM" --> -1
	//  "1 2 SUM 10 20" --> 3
	//  "1 2 abc SUM 10 20" --> 3
	//  "1 2 abc" --> nothing, try again
	//  "1 2" --> nothing, try again

	  System.out.println("Enter a few numbers, devided by spaces, and then 'SUM' text for suming up the entered numbers: ");
	  BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
	  String inputLine = bufferedReader.readLine();

	//  Validate input
	  if (!inputLine.contains(SUM)) {
	   System.out.println("No 'SUM' entered, please try again");
	   return;
	  }

	  int summingResult = 0;
	  String enteredValue;
	  
	  StringTokenizer st = new StringTokenizer(inputLine, " ");
	  while (st.hasMoreTokens()) {
	   enteredValue = st.nextToken();
	   try {
	    summingResult += Integer.parseInt(enteredValue);
	   } catch (NumberFormatException e) {
	    if (enteredValue.equals(SUM)) {
	     System.out.println("Final resut: " + summingResult);
	     break;
	    }
	   }
	  }

	 }
	}
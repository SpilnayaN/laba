package lab3thirdtask;
import java.util.Scanner;
public class Labaratory3_3 {

	public static void main(String[] args) {              //Third task
		System.out.println("Please enter a number: ");
		
		Scanner in = new Scanner(System.in);
        int a = in.nextInt();         //scanning the number
        System.out.println("Prime numbers in the array of [1-" + a + "] are:");
        
		int  x, y;
        for (x = 2; x <=a; x++) {
            y = 0;
            for (int i = 1; i <= x; i++) {
                if (x % i == 0)
                    y++;
            }
            if (y <= 2)
            	System.out.println(x);
 
        }
    }
}
@stackoverflowTest
Feature: check Stackoverflow site

  Scenario: check count is more than three hundred
    Given I on Stackoverflow start page
    Then I check that featured count is more than three hundred

  Scenario: check question was asked today
    Given I on Stackoverflow start page
    When I navigate to question page
    Then I check that question was asked today

  Scenario: check Google, Facebook links are displayed
    Given I on Stackoverflow start page
    When I navigate to SignUp page
    Then I see Google, Facebook links


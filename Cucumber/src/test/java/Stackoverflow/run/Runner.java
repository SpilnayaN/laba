package Stackoverflow.run;

import Stackoverflow.StackoverflowMainPage;
import Stackoverflow.StackoverflowQuestionPage;
import Stackoverflow.StackoverflowSignUp;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Natasha on 28.06.2016.
 */

@RunWith(Cucumber.class)

@CucumberOptions(
        features="src/test/java/Stackoverflow/features",
        glue="Stackoverflow/steps",
        tags={"@stackoverflowTest"}
)
public class Runner {
    public static WebDriver driver;
    public static StackoverflowMainPage stackoverflowMainPage;
    public static StackoverflowQuestionPage stackoverflowQuestionPage;
    public static StackoverflowSignUp stackoverflowSignUp;

    @BeforeClass
    public static void setUp() {
        driver = new FirefoxDriver();
        driver.get("http://Stackoverflow.com");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        stackoverflowMainPage = new StackoverflowMainPage(driver);
    }

    @AfterClass
    public static void afterMethod() {
        driver.close();
    }
}

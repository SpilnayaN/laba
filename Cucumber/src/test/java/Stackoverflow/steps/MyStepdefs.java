package Stackoverflow.steps;

import Stackoverflow.run.Runner;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertTrue;

/**
 * Created by Natasha on 28.06.2016.
 */
public class MyStepdefs {
    @Given("^I on Stackoverflow start page$")
    public void iOnStackoverflowStartPage() {
        Runner.driver.get("http://stackoverflow.com");
    }

    @Then("^I check that featured count is more than three hundred$")
    public void iCheckThatFeaturedCountIsMoreThanThreeHundred() {
        assertTrue(Runner.stackoverflowMainPage.count.isDisplayed());
        String stringcount = Runner.stackoverflowMainPage.count.getText();
        int count = Integer.parseInt(stringcount);
        assertTrue(count > 300);
    }

    @When("^I navigate to question page$")
    public void iNavigateToQuestionPage() {
        assertTrue(Runner.stackoverflowMainPage.lnk_question.isDisplayed());
        Runner.stackoverflowQuestionPage = Runner.stackoverflowMainPage.openQuestionPage();
    }

    @Then("^I check that question was asked today$")
    public void iCheckThatQuestionWasAskedToday() {
        assertTrue(Runner.stackoverflowQuestionPage.txt_today.isDisplayed());
    }

    @When("^I navigate to SignUp page$")
    public void iNavigateToSignUpPage() {
        assertTrue(Runner.stackoverflowMainPage.lnk_signup.isDisplayed());
        Runner.stackoverflowSignUp = Runner.stackoverflowMainPage.openSignUpPage();
    }

    @Then("^I see Google, Facebook links$")
    public void iSeeGoogleFacebookLinks() {
        assertTrue(Runner.stackoverflowSignUp.lnk_google.isDisplayed());
        assertTrue(Runner.stackoverflowSignUp.lnk_facebook.isDisplayed());
    }
}

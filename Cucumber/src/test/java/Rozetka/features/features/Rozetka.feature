@rozetkaTest
Feature: check Rozetka site

  Scenario: check Logo is displayed
    Given I on Rozetka start page
    Then I see Rozetka logo in the top left corner

  Scenario: check Apple item is displayed
    Given I on Rozetka start page
    Then I see Apple item in list of goods

  Scenario: check item with MP3 is displayed
    Given I on Rozetka start page
    Then I see item with MP in list of goods

  Scenario: check 3 cities are displayed
    Given I on Rozetka start page
    When I navigate to Cities pop-up
    Then I see names of three cities

  Scenario: check count
    Given I on Rozetka start page
    When I navigate to Cart page
    Then I see empty cart



package Rozetka.features.run;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import Rozetka.RozetkaMainPage;
import Rozetka.RozetkaCart;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;

/**
 * Created by Natasha on 13.06.2016.
 */

@RunWith(Cucumber.class)

@CucumberOptions(
      features="src/test/java/Rozetka/features/features",
        glue="Rozetka/features/steps",
        tags={"@rozetkaTest"}
)
public class Runner {
    public static WebDriver driver;
    public static RozetkaMainPage rozetkaMainPage;
    public static RozetkaCart rozetkaCart;

    @BeforeClass
    public static void setUp() {
        driver = new FirefoxDriver();
        driver.get("http://Rozetka.com.ua");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        rozetkaMainPage = new RozetkaMainPage(driver);
    }

    @AfterClass
    public static void afterMethod() {
        driver.close();
    }
}

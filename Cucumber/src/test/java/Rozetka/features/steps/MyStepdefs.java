package Rozetka.features.steps;

import Rozetka.features.run.Runner;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertTrue;

/**
 * Created by Natasha on 16.06.2016.
 */

public class MyStepdefs {
    @Given("^I on Rozetka start page$")
    public void iOnRozetkaStartPage() {
        Runner.driver.get("http://rozetka.com.ua");
    }

    @Then("^I see Rozetka logo in the top left corner$")
    public void iSeeRozetkaLogoInTheTopLeftCorner() {
        assertTrue(Runner.rozetkaMainPage.lnk_maintoptab_logo.isDisplayed());
    }

    @Then("^I see Apple item in list of goods$")
    public void iSeeAppleItemInListOfGoods() {
        assertTrue(Runner.rozetkaMainPage.lnk_maintoptab_apple.isDisplayed());
    }

    @Then("^I see item with MP in list of goods$")
    public void iSeeItemWithMPInListOfGoods(){
        assertTrue(Runner.rozetkaMainPage.lnk_maintoptab_mp.isDisplayed());
    }

    @When("^I navigate to Cities pop-up$")
    public void iNavigateToCitiesPopUp() {
        assertTrue(Runner.rozetkaMainPage.lnk_maintoptab_gorod.isDisplayed());
        Runner.rozetkaMainPage.lnk_maintoptab_gorod.click();
    }

    @Then("^I see names of three cities$")
    public void iSeeNamesOfThreeCities() {
        assertTrue(Runner.rozetkaMainPage.lnk_Kyiv.isDisplayed());
        assertTrue(Runner.rozetkaMainPage.lnk_Odessa.isDisplayed());
        assertTrue(Runner.rozetkaMainPage.lnk_Kharkiv.isDisplayed());
    }

    @When("^I navigate to Cart page$")
    public void iNavigateToCartPage() {
        Runner.rozetkaCart = Runner.rozetkaMainPage.openCartPage();
    }

    @Then("^I see empty cart$")
    public void iSeeEmptyCart() {
        assertTrue(Runner.rozetkaCart.txt_empty.isDisplayed());
    }

}

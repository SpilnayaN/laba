package Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Natasha on 30.05.2016.
 */
public class StackoverflowMainPage {
    private static WebDriver driver;

    public StackoverflowMainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//span[@class='bounty-indicator-tab']")
    public WebElement count;

    @FindBy(xpath = ".//*[@class='question-hyperlink']")
    public WebElement lnk_question;

    public StackoverflowQuestionPage openQuestionPage() {
        lnk_question.click();
        return new StackoverflowQuestionPage(driver);
    }
    @FindBy (xpath = ".//*[@id='tell-me-more']")
    public WebElement lnk_signup;

    public StackoverflowSignUp openSignUpPage() {
        lnk_signup.click();
        return new StackoverflowSignUp(driver);
    }
}
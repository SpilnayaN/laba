package Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Natasha on 30.05.2016.
 */
public class StackoverflowSignUp {
    private static WebDriver driver;

    public StackoverflowSignUp(WebDriver driver) {
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }
    @FindBy (xpath = ".//*[text()='Google']")
    public WebElement lnk_google;

    @FindBy (xpath = ".//*[text()='Facebook']")
    public WebElement lnk_facebook;
    }

package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Natasha on 30.05.2016.
 */
public class RozetkaCart {
    private WebDriver driver;

    public RozetkaCart(WebDriver driver) {
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }
    @FindBy (xpath = ".//*[@class='wrap-cart-empty']")
    public WebElement txt_empty;
}

package laba6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Laba6App {
	public static final String START_FOLDER = "resources-Start-Folder";
	
	private static int sequenceNumber = 1;
	
	
	public Laba6App() {
	}

	public void execute() throws IOException {
		System.out.print("Enter part of file name to be copied (or 'quit' to finish): ");
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		String partOfFileName = bufferedReader.readLine();
		
		while (!partOfFileName.equalsIgnoreCase("quit")) {
			CopyFilesThread copyFilesThread = new CopyFilesThread(partOfFileName, sequenceNumber);
			copyFilesThread.start();
			
			sequenceNumber++;
			System.out.print("Enter part of file name to be copied (or 'quit' to finish): ");
			partOfFileName = bufferedReader.readLine();
		}
		
		System.out.println("Application finished.");
	}

//	Написать программу которая бы копировала файлы содержащие в своем названии текст введенный пользователем 
//	из папки и ее подпапок в папку с таким же именем + номер (Новая папка1, Новая папка2 итд). 
//	Так же программа не должна останавливаться во время копирования - она должна быть доступна для работы. 
//	То есть запустив одно копирование, мы должны мочь запустить второе
	public static void main(String[] args) throws IOException {
		Laba6App app = new Laba6App();
		app.execute();
	}

}

package laba6;

public class CopyFilesThread extends Thread {
	private String fileNamePart;
	private int sequenceNumber;
	
	public CopyFilesThread(String fileNamePart, int sequenceNumber) {
		this.fileNamePart = fileNamePart;
		this.sequenceNumber = sequenceNumber;
		
		System.out.println("About to start copying, fileNamePart: " + fileNamePart + ", sequenceNumber: " + sequenceNumber);
	}
	
	public void run() {
//		TODO: create target folder with sequenceNumber in its name
		
//		TODO: get list of all files starting from START_FOLDER
//		TODO: in a loop over that list - check whether filename passing filter rule by fileNamePart, if so - copy such file to target folder
		
//		Simulate copy process by sleeping this thread for 10 seconds
		try {
			Thread.sleep(10 * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Copying finished, sequenceNumber: " + sequenceNumber);
	}
}

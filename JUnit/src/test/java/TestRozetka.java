import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertTrue;
/**
 * Created by Natasha on 30.05.2016.
 */
public class TestRozetka {
    private static WebDriver driver;

    @Before
    public void setUp() throws Exception{
        driver = new FirefoxDriver();
        driver.get("http://rozetka.com.ua");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }
        @Test
        public void checkLogoIsDisplayed(){

            WebElement lnk_maintoptab_logo = driver.findElement(By.xpath("//*[@id='body-header']//div[@class='logo']/img"));
            assertTrue("Cannot find element Logo", lnk_maintoptab_logo.isDisplayed());
        }
        @Test
        public void checkAppleItemIsDisplayed(){

            WebElement lnk_maintoptab_apple = driver.findElement(By.xpath(".//*[@menu_id='2195']"));
            assertTrue("Cannot find item 'Apple'", lnk_maintoptab_apple.isDisplayed());
        }
        @Test
        public void checkItemWithMP3IsDisplayed(){

            WebElement lnk_maintoptab_mp = driver.findElement(By.xpath(".//li[@class='m-main-l-i']/a[contains(text(),'MP3')]"));
            assertTrue("Cannot find item 'MP3'", lnk_maintoptab_mp.isDisplayed());
        }

        @Test
        public void checkCitiesAreDisplayed(){

            WebElement lnk_maintoptab_gorod = driver.findElement(By.xpath(".//*[@class='header-city-select-inner']"));
            lnk_maintoptab_gorod.click();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

            WebElement lnk_Kyiv = driver.findElement(By.xpath(".//*[@locality_id='1']"));
            assertTrue("Cannot find Kyiv", lnk_Kyiv.isDisplayed());
            WebElement lnk_Odessa = driver.findElement(By.xpath(".//*[@locality_id='30']"));
            assertTrue("Cannot find Odessa", lnk_Odessa.isDisplayed());
            WebElement lnk_Kharkiv = driver.findElement(By.xpath(".//*[@locality_id='31']"));
            assertTrue("Cannot find Kharkiv", lnk_Kharkiv.isDisplayed());

        }
        @Test
        public void checkCartIsEmpty(){

            WebElement lnk_korzina = driver.findElement(By.xpath(".//*[@class='sprite-side novisited hub-i-link hub-i-cart-link']"));
            lnk_korzina.click();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

            WebElement txt_empty = driver.findElement(By.xpath(".//*[@class='wrap-cart-empty']"));
            assertTrue("Cart is not empty", txt_empty.isDisplayed());
        }

        @After
        public void afterMethod(){
            driver.close();

        }
    }
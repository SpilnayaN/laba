import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertTrue;
/**
 * Created by Natasha on 31.05.2016.
 */
public class TestStackoverflow {
        private WebDriver driver;

        @Before
        public void setUp() throws Exception{
            driver = new FirefoxDriver();
            driver.get("http://stackoverflow.com");
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        }
        @Test
        public void checkFeaturedCountIsMoreThan300() throws Exception {

            String stringcount=driver.findElement(By.xpath(".//span[@class='bounty-indicator-tab']")).getText();
            int count=Integer.parseInt(stringcount);
            assertTrue("Count is less than 300", count>300);
            }
    @Test
    public void checkGoogleFacebookLinksAreDisplayed() throws Exception {

        WebElement lnk_signup = driver.findElement(By.xpath(".//*[@id='tell-me-more']"));
        lnk_signup.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        WebElement lnk_google = driver.findElement(By.xpath(".//*[text()='Google']"));
        assertTrue("Cannot find element Google", lnk_google.isDisplayed());
        WebElement lnk_facebook = driver.findElement(By.xpath(".//*[text()='Facebook']"));
        assertTrue("Cannot find element Google", lnk_facebook.isDisplayed());
    }

    @Test
    public void checkIfQuestionWasAskedToday() throws Exception {

        WebElement lnk_question = driver.findElement(By.xpath(".//*[@class='question-hyperlink']"));
        lnk_question.click();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        WebElement txt_today = driver.findElement(By.xpath(".//*[@id='qinfo']"));
        assertTrue("Cannot find element Google", txt_today.isDisplayed());
    }

        @After
        public void afterMethod() {
            driver.close();
        }
    }


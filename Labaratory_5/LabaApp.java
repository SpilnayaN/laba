package lab5;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class LabaApp {
	private List<Date> datesList;
	private Random random = new Random();
	private Calendar calendar = Calendar.getInstance();
	
	public LabaApp() {
		datesList = new ArrayList<Date>();
		for (int i = 0; i < 30; i++) {
			long millis = random.nextInt(1000 * 1000 * 1000);
			
			Date d = new Date(millis);
			datesList.add(d);
		}
	}
	
	public void validate() {
		for (Date date : datesList) {
	        calendar.setTime(date);
	        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
	        
			try {
				if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY)
					throw new MyRuntimeException();
				else
					throw new MyException();
			} catch (MyException e) {
				System.out.println(date + " - workday");
			} catch (MyRuntimeException e) {
				System.out.println(date + " - weekend");
			}
		}
	}

	public static void main(String[] args) {
		LabaApp app = new LabaApp();
		app.validate();
	}

}
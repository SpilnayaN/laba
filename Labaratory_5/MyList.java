package lab5methods;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class MyList<E> implements List<E> {
	public Object[] myList;

    MyList(Object[] myList){
        this.myList=myList;
    }

    @Override
	public boolean add(Object e) {
		myList= Arrays.copyOf(myList, myList.length+1);
	       myList[myList.length-1]=e;
	       return true;
	}

	@Override
	public void add(int index, Object element) {
		if (index >= 0 && index < myList.length){
            Object[] copy = new Object[this.myList.length+1];
            System.arraycopy(myList, 0, copy, 0, index);
            copy[index]=element;
            System.arraycopy(myList, index+1, copy, index, myList.length-index);
            myList=copy;
        }

    }


	@Override
	public boolean addAll(Collection c) {
		boolean count=true;
        Object[] a = c.toArray();
        Object[] copy = new Object[myList.length + a.length];
        System.arraycopy(myList, 0, copy, 0, myList.length);
        System.arraycopy(a, 0, copy, myList.length, a.length);
        myList=copy;
        return count;
    	}

	@Override
	public boolean addAll(int index, Collection c) {
		boolean count=true;
        Object[] a = c.toArray();
        Object[] copy = new Object[myList.length + a.length];
        System.arraycopy(myList, 0, copy, 0, index);
        System.arraycopy(a, 0, copy, index, a.length);
        System.arraycopy(myList, index+1, copy, index+a.length, myList.length);
        myList=copy;
        return count;
	}

	@Override
	public void clear() {
		Object[] f = new Object[0];
	    myList=f;
	    }


	@Override
	public boolean contains(Object o) {
		int t=0;
        for(Object d: myList){
            if(d.equals(o)){
                t++;
            }
        }
        if(t>0){
            return true;
        }
        else
            return false;
    }


	@Override
	public boolean containsAll(Collection c) {
		// TODO Auto-generated method stub
		return c.size() == 0;
	}

	@Override
	public E get(int index) {
		return (E) myList[index];
	}

	@Override
	public int indexOf(Object o) {
		int index=-1;
        for (int i = 0; i < myList.length; i++) {
            if(myList[i].equals(o)){
                index=i;
                break;
            }
        }
        return index;
	}

	@Override
	public boolean isEmpty() {
		if(myList.length == 0){
	           return true;
	       }
	        else return false;
	}

	@Override
	public Iterator<E> iterator() {
		return new Iter();
    }

    private class Iter implements Iterator{
    int cursor=0;
        @Override
        public boolean hasNext() {
            if(cursor<myList.length)
                return true;
            else
                return false;
        }

        @Override
        public Object next() {
            cursor++;
            return myList[cursor];
        }
    }


	@Override
	public int lastIndexOf(Object o) {
		int index=-1;
        for (int i = 0; i < myList.length; i++) {
            if(myList[i].equals(o)){
                index=i;
            }
        }
        return index;
	}

	@Override
	public ListIterator<E> listIterator() {
		return new MyIter(0);
	}

	@Override
	public ListIterator<E> listIterator(int index) {
		return new MyIter(index);
	}

	@Override
	public boolean remove(Object o) {
		boolean count=false;
        for (int i = 0; i < myList.length; i++) {
            if(myList[i].equals(o)){
          MyList.this.remove(i);
                break;
            }
        }
        return count;
    }


	@Override
	public E remove(int index) {
		if (index >= 0 && index < myList.length)
        {
            Object[] copy = new Object[this.myList.length-1];
            System.arraycopy(myList, 0, copy, 0, index);
            System.arraycopy(myList, index+1, copy, index, myList.length-index-1);
            myList=copy;
        }
        return null;
    }


	@Override
	public boolean removeAll(Collection c) {
		for(Object o: c){
	           for (int i = 0; i < myList.length; i++) {
	               if(o.equals(myList[i])){
	                   MyList.this.remove(i);
	                   i--;
	               }
	           }
	       }
	       return true;
	    }


	@Override
	public boolean retainAll(Collection<?> c) {
		for (int i = 0; i < myList.length; i++) {
            int tmp=0;
            for (Object o: c){
                if(o.equals(myList[i])){
                    tmp++;
                }
            }
            if(tmp==0){
                MyList.this.remove(i);
            }
        }
        return true;
	}

	@Override
	public Object set(int index, Object element) {
		myList[index]=element;
		return null;
	}

	@Override
	public int size() {
		return myList.length;
    }


	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		Object[] copy = new Object[toIndex-fromIndex];
        System.arraycopy(myList, fromIndex, copy, 0, toIndex);
        return new MyList(copy);
    }


	@Override
	public Object[] toArray() {
		return new Object[0];
	}

	@Override
	public <T> T[] toArray(T[] a) {
		if (a.length != 0) {
		    a[0] = null;
		}
		return a;
	}

}

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertTrue;

/**
 * Created by Natasha on 30.05.2016.
 */
public class TestRozetka {
    private static WebDriver driver;
    public static RozetkaMainPage rozetkaMainPage;
    public static RozetkaCart rozetkaCart;

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get("http://rozetka.com.ua");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        rozetkaMainPage = new RozetkaMainPage(driver);
        }

    @After
    public void afterMethod() {
        driver.close();
    }

    @Test
    public void checkLogoIsDisplayed() {
        assertTrue("Cannot find element Logo", rozetkaMainPage.lnk_maintoptab_logo.isDisplayed());
    }

    @Test
    public void checkAppleItemIsDisplayed() {
        assertTrue("Cannot find item 'Apple'", rozetkaMainPage.lnk_maintoptab_apple.isDisplayed());
    }

    @Test
    public void checkItemWithMP3IsDisplayed() {
        assertTrue("Cannot find item 'MP3'", rozetkaMainPage.lnk_maintoptab_mp.isDisplayed());
    }

    @Test
    public void checkCitiesAreDisplayed() {
        rozetkaMainPage.lnk_maintoptab_gorod.click();
        assertTrue("Cannot find Kyiv", rozetkaMainPage.lnk_Kyiv.isDisplayed());
        assertTrue("Cannot find Odessa", rozetkaMainPage.lnk_Odessa.isDisplayed());
        assertTrue("Cannot find Kharkiv", rozetkaMainPage.lnk_Kharkiv.isDisplayed());

    }

    @Test
    public void checkCartIsEmpty() {
        rozetkaCart=rozetkaMainPage.openCartPage();
        assertTrue("Cart is not empty", rozetkaCart.txt_empty.isDisplayed());
    }
}
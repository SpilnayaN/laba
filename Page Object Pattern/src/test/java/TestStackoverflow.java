import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertTrue;

/**
 * Created by Natasha on 31.05.2016.
 */
public class TestStackoverflow {
    private WebDriver driver;
    public static StackoverflowMainPage stackoverflowMainPage;
    public static StackoverflowQuestionPage stackoverflowQuestionPage;
    public static StackoverflowSignUp stackoverflowSignUp;

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get("http://stackoverflow.com");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        stackoverflowMainPage = new StackoverflowMainPage(driver);
       }
    @After
    public void afterMethod() {
        driver.close();
    }

    @Test
    public void checkFeaturedCountIsMoreThan300() {
        String stringcount = stackoverflowMainPage.count.getText();
        int count = Integer.parseInt(stringcount);
        assertTrue("Count is less than 300", count > 300);
    }
    @Test
    public void checkIfQuestionWasAskedToday() {
        stackoverflowQuestionPage=stackoverflowMainPage.openQuestionPage();
        assertTrue("Cannot find element Google", stackoverflowQuestionPage.txt_today.isDisplayed());
    }
    @Test
    public void checkGoogleFacebookLinksAreDisplayed() {
        stackoverflowSignUp=stackoverflowMainPage.openSignUpPage();
        assertTrue("Cannot find element Google", stackoverflowSignUp.lnk_google.isDisplayed());
        assertTrue("Cannot find element Google", stackoverflowSignUp.lnk_facebook.isDisplayed());
    }
}




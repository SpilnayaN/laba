import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Natasha on 30.05.2016.
 */
public class StackoverflowQuestionPage {
    private static WebDriver driver;

    public StackoverflowQuestionPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }
    @FindBy (xpath = ".//*[@id='qinfo']")
    public WebElement txt_today;
}

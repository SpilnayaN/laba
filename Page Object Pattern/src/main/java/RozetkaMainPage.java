import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Natasha on 30.05.2016.
 */
public class RozetkaMainPage {
    private static WebDriver driver;

    public RozetkaMainPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }
    @FindBy (xpath = "//*[@id='body-header']//div[@class='logo']/img")
    public WebElement lnk_maintoptab_logo;

    @FindBy (xpath = ".//*[@menu_id='2195']")
    public WebElement lnk_maintoptab_apple;

    @FindBy (xpath = ".//li[@class='m-main-l-i']/a[contains(text(),'MP3')]")
    public WebElement lnk_maintoptab_mp;

    @FindBy (xpath = ".//*[@class='header-city-select-inner']")
    public WebElement lnk_maintoptab_gorod;

    @FindBy (xpath = ".//*[@locality_id='1']")
    public WebElement lnk_Kyiv;

    @FindBy (xpath = ".//*[@locality_id='30']")
    public WebElement lnk_Odessa;

    @FindBy (xpath = ".//*[@locality_id='31']")
    public WebElement lnk_Kharkiv;

    @FindBy (xpath = ".//*[@class='sprite-side novisited hub-i-link hub-i-cart-link']")
    public WebElement lnk_korzina;

    public RozetkaCart openCartPage() {
        lnk_korzina.click();
        return new RozetkaCart(driver);
    }
}

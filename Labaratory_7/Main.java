package lab7;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {

	public static void main(String[] args) throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		
		Human human = new Human("Vasya", 25);
		
		  try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("D:\\Test\\Human.data"))){
	           oos.writeObject(human);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }

		  Object object = null;
		  
	        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("D:\\Test\\Human.data")))){
	        	object = ois.readObject();
	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        }
	        
	        System.out.println(object);
	        
	        Method method = object.getClass().getMethod("rename", String.class);
	        method.invoke(object, "Kolya");
	        System.out.println(object);
	}

}

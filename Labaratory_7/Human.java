package lab7;

import java.io.Serializable;

public class Human implements Serializable {

	private static final long serialVersionUID = 5151951515259148L;
	
	private String name;
	private int age;
	
	public Human(String name, int age) {
		setName(name);
		setAge(age);
	}
	
	public void rename(String newName){
		setName(newName);
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Human [name=" + name + ", age=" + age + "]";
	}
}

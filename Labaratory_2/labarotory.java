package Lab1;

public class labarotory {

	public static void main(String[] args) {
		System.out.println("1.1 Arithmetical operations:" ); // First task
		
		int a = 100;
		int b = 200;
		
		int sum = a + b;
		int sub = b - a;
		int mult = a * b;
		int div = b / a;
		int mod=b%a;
		int postInc = a++;
		int prefInt = ++a;
							
		System.out.println("100 + 200 = " + sum);
		System.out.println("200 - 100 = " + sub);
		System.out.println("100 * 200 = " + mult);
		System.out.println("200 / 100 = " + div);
		System.out.println("200 % 100 = " + mod);
		System.out.println("a++ = " + postInc);
		System.out.println("++a = " + prefInt);
		
		System.out.println("1.2 Comparing operations:" );
			
		boolean comp1 = a >= b;
		boolean comp2 = a == b;
		boolean comp3 = a != b;
		boolean comp4 = (3 > 5)&&(10 > 8);
		boolean comp5 = (3 > 5)|| (10 > 8);
				
		System.out.println("100 >= 200= " + comp1);
		System.out.println("100 == 200 = " + comp2);
		System.out.println("100 != 200 = " + comp3);
		System.out.println("(3 > 5) && (10 > 8) = " + comp4);
		System.out.println("(3 > 5)|| (10 > 8) = " + comp5);
		
		System.out.println("1.3 Logical operations:" );
		System.out.println("if...else" );
		
		if (100 == 200)
		    System.out.println("100 and 200 - equal numbers");
		else
		    System.out.println("100 and 200 - are not equal numbers");
		
		System.out.println("switch" );
		
		int mark = 5;
	    String markString;
	    switch (mark) {
	        case 1:  markString = "The worst mark";
	                 break;
	        case 2:  markString = "Very bad mark";
	                 break;
	        case 3:  markString = "Bad mark";
	                 break;
	        case 4:  markString = "Not so bad, but could be better";
	                 break;
	        case 5:  markString = "Perfect, good job!";
	                 break;
	        default: markString = "Nothing was input";
	                 break;
	    }
	    System.out.print(markString);
	    
	    	    
	    System.out.println("1.4 Assignment of values:" );
	    int x, y, z;
	    x = y = z = 9;
	    long ln =59999998L;
	    double d = 0.25678;
	    float fl = 44.03F;
	    char s = 's';
	    String str = "String";
	    
	    System.out.println("int x, y, z = " + x);
	    System.out.println("long l = " + ln);
	    System.out.println("double d = " + d);
	    System.out.println("float f = " + fl);
	    System.out.println("char s = " + s);
	    System.out.println("String str0 = " + str);
		
				
		System.out.println("--------------------------------"); // Second task
		System.out.println("2.Convertation from primitive types to String and vice versa:"); 
		int f = 135;
		   String str1 = Integer.toString(f);
		double  g = 32.4;
		   String str2 = Double.toString(g);
		char ch = 74;
		   String str3 = Character.toString(ch);
		   
		System.out.println("int to String = " + str1);
		System.out.println("double to String = " + str2); 
		System.out.println("char to String = " + str3);
		
		int k = 0;
	    String str4 = "102944";
	    try {
	        k = Integer.parseInt(str4);
	        System.out.println("String to int = " + k);  
	    } catch (NumberFormatException k1) {  
	        System.err.println("Incorrect format");  
	    }     
	    double l = 0;
	    String str5 = "13.7";
	    try {
	        l = Double.parseDouble(str5);
	        System.out.println("String to double = " + l);
	    } catch (NumberFormatException k1) {
	        System.err.println("Incorrect format");
	    }                            
	    String s1 = "True";
	    boolean boolean1;

	    boolean1 = Boolean.parseBoolean(s1);
	    System.out.println("String to boolean = " + boolean1 );
	    
		

	    System.out.println("--------------------------------");	 // Third task   	
	    System.out.println("3. Convertation from integer to double/float and vice versa:");
	    int number = 123;
	    float intToFloat = (float)number;
	    double number1 = 3.14d;
	    int doubleToInt = (int) number1;
					
	    System.out.println("int to float = " + intToFloat);
	    System.out.println("double to int = " + doubleToInt);
		}
	}

	
			   		







package lab4;

import java.time.LocalTime;
import java.util.Arrays;

public class App {
	public static void main(String[] args) {
		
		
		int[][] car = { {1, new CarA(10, 0.1, 2).race(20, 2000)},
				{2,new CarB(100, 0.1, 2).race(20, 2000)},
				{3,new CarC(100, 0.1, 2).race(20, 2000)}};
		
		sort(car);	
		
		System.out.println("Car # " + car[0][0] + " came in " + LocalTime.MIN.plusSeconds(car[0][1]).toString());
		
//		for (int[] is : car) {
//			System.out.println("Car # " + is[0] + " came in " + LocalTime.MIN.plusSeconds(is[1]).toString());
//		}
		
	}
	public static void sort (int[][] arr) {
	    for (int i = 0; i < arr.length - 1; i++) {
	        boolean swapped = false;
	        for (int j = 0; j < arr.length - i - 1; j++) {
	            if (arr[j][1] > arr[j + 1][1]) {
	                int tmp[] = arr[j];
	                arr[j] = arr[j + 1];
	                arr[j + 1] = tmp;
	                swapped = true;
	            }
	        }            
	        
	        if(!swapped)
	            break;
	    }
	}
}

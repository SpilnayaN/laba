package lab4;

public class CarC extends Car {

	public CarC(int vmax, double m, int a) {
		super(vmax, m, a);
	}
	@Override
	public int race(int distance, int length) {
		int time = 0;
		for (int i = 0; i < distance; i++) {
			time += drive(length, getA());
			
			if(getV() == getVmax()){
				setVmax((int)(getVmax()*1.1));
			}
			
			turn(getM());
		}
		return time;
	}
}
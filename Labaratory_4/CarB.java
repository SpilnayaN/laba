package lab4;

public class CarB extends Car {

	public CarB(int vmax, double m, int a) {
		super(vmax, m, a);
	}

	@Override
	public int race(int distance, int length) {
		int time = 0;
		for (int i = 0; i < distance; i++) {
			if (getV() < getVmax() / 2 && i != 0) {
				time += drive(length, getA()*2);
			} else {
				time += drive(length, getA());
			}
			turn(getM());
		}
		return time;
	}
}
package lab4;

import static java.lang.Math.sqrt;

public class Car {
	private int vmax;        //max speed
	private double m;        //mobility, maneuverability
	private int a;           //acceleration
	private int v;           //current speed
	
	public Car(int vmax, double m, int a) {
		setVmax(vmax*1000/3600);
		setM(m);
		setA(a);
	}
	
	public int drive(int s, int a){
		int v0 = v;
		int t = 0;
		v = (int)sqrt(v0*v0 + 2*a*s);
		if(v <= vmax){
			t = (v - v0)/a;
		}else{
			v = vmax;
			int st = (v*v - v0*v0)/2*a;
			t = (v - v0)/a + (s - st)/v;
		}
		return t;
	}
	
	public int turn(double m){
		return v = (int)(v * m);
	}
	
	public int race(int distance, int length){
		int time = 0;
		for (int i = 0; i < distance; i++) {
			time += drive(length, getA());
			turn(m);
		}
		return time;
	}
	
	
	public double getM() {
		return m;
	}
	public void setM(double m) {
		this.m = m;
	}
	public int getA() {
		return a;
	}
	public void setA(int a) {
		this.a = a;
	}

	public int getVmax() {
		return vmax;
	}

	public void setVmax(int vmax) {
		this.vmax = vmax;
	}

	public int getV() {
		return v;
	}

	public void setV(int v) {
		this.v = v;
	}

}
package lab4;

public class CarA extends Car {

	public CarA(int vmax, double m, int a) {
		super(vmax, m, a);
	}
	
	@Override
	public int race(int distance, int length) {
		int time = 0;
		for (int i = 0; i < distance; i++) {
			time += drive(length, getA());
			if (getV() > getVmax() / 2) {
				turn(getM() + (getV() - getVmax() / 2)*0.005);
			} else {
				turn(getM());
			}
		}
		return time;
	}

}
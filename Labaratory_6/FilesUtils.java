package lab6;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FilesUtils {
	public static void copyFiles(final String name, final String dir){
		final File srcDir = new File(dir);
		File disDir = null;
		synchronized(FilesUtils.class) {
			for (int i = 1;; i++) {
				disDir = new File(dir + i);
				if(!disDir.exists()){
					disDir.mkdirs();
					break;
				}
			}
		}
		final File disDirfinal = disDir;
		new Thread(()->{copyDir(name, disDirfinal, srcDir);}).start();
	}

	public static void copyDir(String name, File disDir, File srcDir) {
		File[] fileList = srcDir.listFiles();
		for (File file : fileList) {
			if(file.isFile()){
				if(file.getName().contains(name)){
					copyBuffered(file, new File(disDir + "\\"+ file.getName()));
				}
			}else{
				copyDir(name, disDir, file);
			}
		}
	}
	
	public static void copyBuffered(File from, File to) {
		try {
			Thread.currentThread().sleep(500);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		try (BufferedInputStream br = new BufferedInputStream(new FileInputStream(from));
				BufferedOutputStream bw = new BufferedOutputStream(new FileOutputStream(to))){
			int i = 0;
			
			while ((i = br.read()) != -1) {
				bw.write(i);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}